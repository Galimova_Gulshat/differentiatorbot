﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Diff;
using DiffListener;

namespace MainClass
{
    class Program
    {
        static void Main(string[] args)
        {
            MyHttpListener.StartListen("localhost", "8080");
            Bot.Start();
        }
    }
}
