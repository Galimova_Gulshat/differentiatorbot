﻿using System;
using NUnitLite;
using AlgebraDiff;
using Telegram.Bot;
using Telegram.Bot.Args;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Text;

namespace Diff
{
    public class Program
    {
        static void Main()
        {
        }
    }
    public class Bot
    { 
        static ITelegramBotClient botClient;
        static IExpressionParser expressionParser;
        

        public static void Start()
        {
            expressionParser = new DynamicParser();
            var api = "621155111:AAHJR137HRrRblWPuMOWF9V3u79Ylz4bJWA";
            botClient = ProxyReceiver.GetSOCKS5Proxy(api);
            //botClient = new TelegramBotClient(api);
            var me = botClient.GetMeAsync().Result;
            botClient.SendTextMessageAsync(
                   chatId: botClient.MessageOffset,
                   text: expressionParser.Rules
                 );
            Console.WriteLine(
              $"Hello, World! I am user {me.Id} and my name is {me.FirstName}."
            );

            botClient.OnMessage += Bot_OnMessage;
            botClient.StartReceiving();
            Thread.Sleep(int.MaxValue);
        }

        static async void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            if (e.Message.Text != null)
            {
                Console.WriteLine($"Received a text message in chat {e.Message.Chat.Id}.");
                if (IsrulesNeed(e.Message.Text, "rule", "help", "правил", "помо"))
                    await botClient.SendTextMessageAsync(
                  chatId: e.Message.Chat,
                  text: expressionParser.Rules
                );
                else
                {
                    var result = string.Empty;
                    try
                    {
                        result = GetDiff(e.Message.Text).Result;
                    }
                    catch (Exception exp)
                    {
                        if (exp != null)
                            result = "Sorry :( Server unconnected";
                    }
                    await botClient.SendTextMessageAsync(
                      chatId: e.Message.Chat,
                      text: result
                    );
                }
            }
        }

        static bool IsrulesNeed(string str, params string[] words)
        {
            for (int i = 0; i < words.Length; i++)
                if (str.Contains(words[i]))
                    return true;
            return false;
        }

        static async Task<string> GetDiff(string function)
        {
            HttpWebRequest request = WebRequest.Create($"http://localhost:8080/?Function=" + function) as HttpWebRequest;

            HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse;
            WebHeaderCollection header = response.Headers;
            string responseString;
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), ASCIIEncoding.ASCII))
            {
                responseString = reader.ReadToEnd();
            }

            if (responseString == null)
                return null;

            return responseString;
        }
    }
}
