﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DiffListener
{
    public class Program
    {
        static void Main()
        {
        }
    }
    public class MyHttpListener
    {
        private string url;
        private bool isListening = false;
        private HttpListener listener = new HttpListener();

        public static void StartListen(string port, string host)
        {
            var listen = new MyHttpListener(port, host);
            listen.Start();
        }
        public MyHttpListener(string port, string host)
        {
            url = $"http://{port}:{host}/";
        }

        public MyHttpListener()
        {
            url = "http://localhost:8080/";
        }

        public async void Start()
        {
            listener.Prefixes.Add(url);
            listener.Start();

            isListening = true;

            while (isListening)
            {
                HttpListenerContext context = await listener.GetContextAsync();

                HttpListenerRequest request = context.Request;

                HttpListenerResponse response = context.Response;

                
                if (!isListening)
                    break;
                var func = request.QueryString["Function"];
                var result = await GetResultAsync(func);
                var buffer = Encoding.UTF8.GetBytes(result);
                response.ContentLength64 = buffer.Length;
                response.OutputStream.Write(buffer, 0, buffer.Length);
                response.OutputStream.Close();

                Console.WriteLine("{0} request was caught: {1}",
                                   request.HttpMethod, request.Url);

                response.StatusCode = (int)HttpStatusCode.OK;
                using (Stream stream = response.OutputStream) { }
                ReadConsole();
            }
        }

        public async void ReadConsole()
        {
             await Task.Run(() =>
            { 
                var read = Console.ReadLine();
                if (read.Contains("top"))
                {
                    isListening = false;
                }
            });
        }

        public async Task<string> GetResultAsync(string function)
        {
            if (function == null)
                return "Incorrect data";
            var diffFunc = AlgebraDiff.Algebra.DifferentiateString(function);
            return await Task.Run(() => diffFunc == null ? "Incorrect data" : diffFunc.ToString());
        }

        public void StopListen()
        {
            isListening = false;
            listener.Stop();
            listener.Close();
        }
    }
}
