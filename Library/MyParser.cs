﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlgebraDiff
{
    class MyParser : IExpressionParser
    {
        public string Rules => "Сперва укажи параметр, отдели его от выражения любым знаком, а потом пиши выражение! У меня нет обозначения для степени, поэтому пиши умножением! И отделяй все пробелами)";
        private ParameterExpression param;

        public Expression<Func<double, double>> GetExpression(string str)
        {
            var strs = str.Replace(".", ",").Split(' ');
            if (strs.Length < 3)
                return null;
            param = Expression.Parameter(typeof(double), strs[0]);
            LambdaExpression exp = Expression.Lambda(Expression.Default(typeof(double)));
            try
            {
                exp = Expression.Lambda<Func<double, double>>(ExpressionCall(strs), new ParameterExpression[] { param });
            }
            catch (Exception e)
            {
                if (e != null)
                    return null;
            }
            return (Expression<Func<double, double>>)exp;
        }

        private Expression ExpressionCall(string[] strs)
        {
            var isAll = Tuple.Create(false, 2);
            var list = new List<Expression>();
            var sIndex = 2;
            for (int i = 2; i < strs.Length; i++)
            {
                if (strs[i] == "*")
                {
                    isAll = Tuple.Create(true, i + 1);
                    GetMultiplyExpression(strs, ref i, list, ref sIndex);
                }
                else if (IsCosOrSinFunction(strs[i]))
                {
                    isAll = Tuple.Create(true, i + 1);
                    list.Add(CallExpression(strs, ref i, strs[i].Contains("o") ? "Cos" : "Sin"));
                }
                else if (strs[i] == "+")
                    isAll = Tuple.Create(false, i + 1);
            }
            if (list.Count == 0)
            {
                list.Add(GetExp(strs, 2, strs.Length));
                isAll = Tuple.Create(true, 0);
            }
            if (!isAll.Item1)
                list.Add(GetExp(strs, isAll.Item2, strs.Length));
            Expression ex = list[0];
            for (int i = 1; i < list.Count; i++)
                ex = Expression.Add(ex, list[i]);
            return ex;
        }

        private Expression GetExp(string[] strs, int start, int end)
        {
            if (end - start == 1)
            {
                if (strs[start] == param.Name)
                    return param;
                double r;
                if (Double.TryParse(strs[start], out r))
                    return Expression.Constant(Double.Parse(strs[start]));
                if (IsCosOrSinFunction(strs[start]))
                    return CallExpression(strs, ref start, strs[start].Contains("o") ? "Cos" : "Sin");
                return null;
            }
            for (int i = start; i < end; i++)
            {
                if (strs[i] == "+")
                    return Expression.Add(GetExp(strs, start, i), GetExp(strs, i + 1, end));
                if (strs[i] == "*")
                    return Expression.Multiply(GetExp(strs, start, i), GetExp(strs, i + 1, end));
                if (IsCosOrSinFunction(strs[start]))
                    return CallExpression(strs, ref start, strs[i].Contains("o") ? "Cos" : "Sin");
            }
            return Expression.Lambda<Func<double, double>>(param, param);
        }

        private bool IsCosOrSinFunction(string str) =>
            str.Contains("cos") || str.Contains("Cos") || str.Contains("sin") || str.Contains("Sin");

        private Expression CallExpression(string[] strs, ref int start, string name) =>
            Expression.Call(typeof(Math).GetMethod(name, new Type[] { typeof(double) }),
                new Expression[] { ExpressionInS(strs, ref start) });

        private Expression ExpressionInS(string[] strs, ref int start)
        {
            var cosValues = new List<string>() { "", "" };
            var value = strs[start].Split('(', ')')[1];
            cosValues.Add(value);
            for (int j = start + 1; j < strs.Length; j++)
            {
                if (strs[j].Contains(")"))
                {
                    cosValues.Add(strs[j].Split(')')[0]);
                    start = j;
                    break;
                }
                cosValues.Add(strs[j]);
            }
            return ExpressionCall(cosValues.ToArray());
        }

        private void GetMultiplyExpression(string[] strs, ref int i, List<Expression> list, ref int sIndex)
        {
            if (strs[i + 1].Contains("("))
            {
                i++;
                if (IsCosOrSinFunction(strs[i]))
                    list.Add(Expression.Multiply(GetExp(strs, sIndex, i - 1), CallExpression(strs, ref i, strs[i].Contains("o") ? "Cos" : "Sin")));
                else
                    list.Add(Expression.Multiply(GetExp(strs, sIndex, i - 1), ExpressionInS(strs, ref i)));
            }
            else
            {
                var eIndex = i;
                while (eIndex < strs.Length && strs[eIndex] != "+")
                    eIndex++;
                list.Add(Expression.Multiply(GetExp(strs, sIndex, i), GetExp(strs, i + 1, eIndex)));
                sIndex = eIndex + 1;
                i = eIndex - 1;
            }
        }
    }
}
