﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlgebraDiff
{
    public interface IExpressionParser
    {
        string Rules { get; }
        Expression<Func<double, double>> GetExpression(string str);
    }
}
