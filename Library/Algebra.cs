﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AlgebraDiff
{
    public static class Algebra
    {
        private static IEnumerable<ParameterExpression> parameters;

        public static Expression<Func<double, double>> Differentiate(Expression<Func<double, double>> func)
        {
            if (func == null)
                return null;
            parameters = func.Parameters;
            return DifferentiateHelper(func.Body);
        }

        private static Expression<Func<double, double>> DifferentiateHelper
            (Expression body)
        {
            if (body is ConstantExpression)
                return GetLambda(Expression.Constant(0.0, typeof(double)));

            if (body is ParameterExpression)
                return GetLambda(Expression.Constant(1.0, typeof(double)));

            if (body is BinaryExpression binaryExpression)
                return DifferentiateBinary(binaryExpression);

            if (body is MethodCallExpression callExpression)
            {
                var parameter = callExpression.Arguments[0];
                var methodName = callExpression.Method.Name;
                var call = GetCallExpression("Cos", parameter);
                switch (methodName)
                {
                    case "Sin":
                        break;
                    case "Cos":
                        call = Expression.Negate(GetCallExpression("Sin", parameter));
                        break;
                }
                return GetLambda(GetExpression(Expression.Multiply(call, GetExpression(DifferentiateHelper(parameter).Body))));
            }
            return null;
        }

        private static Expression<Func<double, double>> GetLambda(Expression expression) =>
            Expression.Lambda<Func<double, double>>(expression, parameters);

        private static Expression GetCallExpression(string name, Expression parameter) =>
            Expression.Call(typeof(Math).GetMethod(name, new Type[] { typeof(double) }), parameter);

        private static Expression GetExpression(Expression body)
        {
            if (body is BinaryExpression binaryExpression)
            {
                var firstArg = binaryExpression.Left;
                var secondArg = binaryExpression.Right;
                if (binaryExpression.NodeType.Equals(ExpressionType.Multiply))
                {
                    var result = PrepareMultiply(firstArg, secondArg) == null ? PrepareMultiply(firstArg, secondArg) : PrepareMultiply(secondArg, firstArg);
                    if (result != null)
                        return result;
                    if (firstArg is ConstantExpression first && secondArg is ConstantExpression second)
                        return Expression.Constant((double)first.Value * (double)second.Value, typeof(double));
                }
                if (binaryExpression.NodeType.Equals(ExpressionType.Add))
                {
                    if (IsConstantZero(firstArg))
                        return secondArg;
                    if (IsConstantZero(secondArg))
                        return firstArg;
                    if (firstArg is ConstantExpression first && secondArg is ConstantExpression second)
                        return Expression.Constant((double)first.Value + (double)second.Value, typeof(double));
                    if (firstArg.ToString().Equals(secondArg.ToString()))
                        return Expression.Multiply(firstArg, Expression.Constant(2.0, typeof(double)));
                }
            }
            return body;
        }

        private static Expression<Func<double, double>> DifferentiateBinary(BinaryExpression binaryExpression)
        {
            var nodeType = binaryExpression.NodeType;
            var firstArg = binaryExpression.Left;
            var secondArg = binaryExpression.Right;
            var firstArgDif = DifferentiateHelper(firstArg).Body;
            var secondArgDif = DifferentiateHelper(secondArg).Body;

            if (nodeType.Equals(ExpressionType.Multiply))
                return GetLambda(GetExpression(Expression.Add(GetExpression(Expression.Multiply(firstArgDif, secondArg)),
                                    GetExpression(Expression.Multiply(firstArg, secondArgDif)))));

            if (nodeType.Equals(ExpressionType.Add))
                return GetLambda(GetExpression(Expression.Add(firstArgDif, secondArgDif)));
            return null;
        }

        private static bool DoubleEquals(double firstValue, double secondValue) => Math.Abs(firstValue - secondValue) < Double.Epsilon;

        private static Expression PrepareMultiply(Expression first, Expression second)
        {
            if (first is ConstantExpression firstConstant)
            {
                if (DoubleEquals((double)firstConstant.Value, 1.0))
                    return second;
                if (DoubleEquals((double)firstConstant.Value, 0.0))
                    return Expression.Constant(0.0, typeof(double));
            }
            return null;
        }

        private static bool IsConstantZero(Expression expression) =>
            expression is ConstantExpression constant && DoubleEquals((double)constant.Value, 0.0);
    }
}
