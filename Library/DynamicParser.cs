﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlgebraDiff
{
    class DynamicParser : IExpressionParser
    {
        public string Rules => "Просто введите выражение! У меня нет обозначения для степени, поэтому пиши умножением! Пиши 1.5, а не 1,5";

        public Expression<Func<double, double>> GetExpression(string str)
        {
            str = str.Replace("Cos", "Math.Cos").Replace("cos", "Math.Cos")
                .Replace("Sin", "Math.Sin").Replace("sin", "Math.Sin").Replace(",", ".");
            ParameterExpression param = Expression.Parameter(typeof(double), "x");
            if (str.Length == 1 && Char.IsDigit(str[0]))
                str = str + ".0";
            var stop = false;
            if (!str.Contains("."))
                for (int i = 0; i < str.Length; i++)
                {
                    if (stop)
                        break;
                    if (Char.IsDigit(str[i]))
                    {
                        for (int j = i + 1; j < str.Length; j++)
                        {
                            if (Char.IsDigit(str[j]))
                                continue;
                            else
                            {
                                str = str.Replace(str[i].ToString(), str[i].ToString() + ".0");
                                stop = true;
                                break;
                            }
                        }
                    }
                }
            for (int i = str.Length - 1; i >= 0; i--)
            {
                if (Char.IsLetter(str[i]))
                {
                    param = Expression.Parameter(typeof(double), str[i].ToString());
                    break;
                }
            }
            LambdaExpression exp = Expression.Lambda(Expression.Default(typeof(double)));
            try
            {
                exp = (Expression<Func<double, double>>)(System.Linq.Dynamic.DynamicExpression.ParseLambda(new[] { param }, null, str));
            }
            catch (Exception e)
            {
                if (e != null)
                    return null;
            }
            return (Expression<Func<double, double>>)exp;
        }
    }
}
